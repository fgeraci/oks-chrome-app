chrome.app.runtime.onLaunched.addListener(function() {
  
  var screenWidth = screen.width;
  var screenHeight = screen.height;
  var windowWidth = 500;
  var windowHeight = 600;
  var options = {
    'bounds': {
      'width': 300,
      'height': 400,
      'left': Math.round((screenWidth-windowWidth)/2),
	  'top': Math.round((screenHeight-windowHeight)/2)
    },
    'resizable': false
  }
	
		
	chrome.app.window.create("view/main.html", options);
});


/* FACILITIES */

function increaseOpacity(event) {
  var el = (event.target || event.srcElement);
  var op = window.getComputedStyle(el,null).opacity;
  if(Number(op) < 1.0) {
    var val = Number(op)+0.05;
    el.style.opacity = val.toString();
    setTimeout(function(){increaseOpacity(event)},50);
  }
  
}

function decreaseOpacity(event) {
  var el = (event.target || event.srcElement);
  var op = window.getComputedStyle(el,null).opacity;
  if(Number(op) > 0.5) {
    var val = Number(op)-0.05;
    el.style.opacity = val.toString();
    setTimeout(function(){decreaseOpacity(event)},50);
  }
}

function displayErrorBox(el) {
  el.style.opacity = "1.0";
  fadeOut(el); 
}

function errorNotification(error) {
  delete window.alert;
  alert(error);
}

function fadeOut(el) {
  var op = el.style.opacity;
  if(Number(op) > 0.0) {
    var opN = Number(op) - 0.01;    
    el.style.opacity = (opN).toString();
    setTimeout(function() {fadeOut(el);}, 50);
  }
}
