document.addEventListener('DOMContentLoaded', init); // do not use init() such that the function is not immediately invoked
var interval;

function init() {

  document.getElementById('button').addEventListener('click', function() {
    authenticate();
  });
  
  document.getElementById('password').addEventListener('keydown', function(event) {
    if(event.which == 13) {
      authenticate();
    }
  });

  document.getElementById('button').addEventListener('mouseover',
                                                     function(event){increaseOpacity(event)},
                                                     false);
  document.getElementById('button').addEventListener('mouseout',
                                                     function(event){decreaseOpacity(event)},
                                                     false);
}

function validateCredentials() {
  var user = document.getElementById('user').value;
  var password = document.getElementById('password').value;
  if(user.length > 0 && password.length > 0) return true;
  else {
    if(user.length > 0) displayError("Please type your password ...");
    else if(password.length > 0) displayError("Please type your username ...");
    return false;
  }
  
}

function authenticate() {
  if(validateCredentials()) {
    var user = document.getElementById("user").value;
    var password = document.getElementById("password").value;
    
    try {
      connectToLdap();
    } catch(err) {
      displayError(err);
    }
  }
}

function displayError(error) {
  displayErrorBox(document.getElementById('errorBox'));
}

function connectToLdap() {
  
  var ldap = require('ldapjs'); // returns the file into the variable as a namespace, specific from Node.js
  
  ldap.Attribute.settings.guid_format = ldap.GUIS_FORMAT_B;
  /*  
  var client = ldap.createClient(
      { url: 'ldap://50.203.84.41' });
  var opts = {
    filter: '(objectclass=user)',
    scope: 'sub',
    attributes: ['samaccountname']};
    
  // need to investigate this.
  client.bind('cn=ldapcheck2,cn=Users,dc=oakknoll,dc=org', 'OKS44ldapch3ck', function(err) {
    client.search('CN=test,OU=Development,DC=Home', opts, function (err, search) {
      search.on('searchEntry', function (entry) {
        var user = entry.object;
        console.log(user.objectGUID);
      });
    });
  });
  */
}
